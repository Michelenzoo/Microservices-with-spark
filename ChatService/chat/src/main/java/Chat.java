import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import static spark.Spark.get;

/**
 * Created by michel on 10-6-2016.
 */

public class Chat {
    public static void main(String[] args){
        ChatService chatService = new ChatService();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        CorsFilter.apply();
        get("/user", (req, res) -> chatService.user(req.queryParams("name"), req.queryParams("password")), gson::toJson);
        get("/start", (req, res) -> chatService.start(req.queryParams("sid"), req.queryParams("rid")), gson::toJson);
        get("/getChats", (req, res) -> chatService.getChats(req.queryParams("uid")), gson::toJson);
        get("/send", (req, res) -> chatService.send(req.queryParams("cid"), req.queryParams("uid"), req.queryParams("message")), gson::toJson);
        get("/getMessages", (req, res) -> chatService.getMessages(req.queryParams("cid")), gson::toJson);
    }
}
