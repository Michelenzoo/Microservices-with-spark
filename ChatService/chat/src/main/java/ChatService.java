import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by michel on 10-6-2016.
 */
public class ChatService {
    private Database db;
    private Exception dbException;
    public ChatService(){
        try {
            db = new Database();
        }catch (Exception e){
            dbException = e;
        }
    }
    public Map user(String name, String password){
        Map<String, String> returnData = new HashMap<>();
        if(name != null && name != "" && password != null && password != ""){
            try {
                Map<String, String> getUser = db.getUser(name, password);
                if("False".equalsIgnoreCase(getUser.get("Check"))){
                    Long userId = db.insertUser(name, password);

                    if(userId != null){
                        returnData.put("Result", "User inserted");
                        returnData.put("Code", "0");
                        returnData.put("UID", Long.toString(userId));
                    } else {
                        returnData.put("Result", "User insert failed");
                        returnData.put("Code", "102");
                    }

                } else {
                    returnData.put("Result", "User exists");
                    returnData.put("Code", "0");
                    returnData.put("UID", getUser.get("UID"));
                }
            } catch(Exception e){
                returnData.put("Result", e.getMessage());
                returnData.put("Code", "101");
            }
            return returnData;
        } else {
            returnData.put("Result", "Empty field given");
            returnData.put("Code", "110");
            return returnData;
        }
    }

    public Map start(String sid, String rid){
        Map<String, String> returnData = new HashMap<>();
        if(sid != null && sid != "" && rid != null && rid != ""){
            try {
                Map<String, String> getChat = db.getChat(sid, rid);
                if("False".equalsIgnoreCase(getChat.get("Check"))){
                    Map<String, String> data = db.insertChat(sid, rid);
                    if("True".equalsIgnoreCase(data.get("Check"))){
                        returnData.put("Result", "Chat started");
                        returnData.put("Code", "0");
                        returnData.put("CID", data.get("CID"));
                        returnData.put("Time_Started", data.get("Time_Started"));
                    } else {
                        returnData.put("Result", "User insert failed");
                        returnData.put("Code", "202");
                    }

                } else {
                    System.out.println("StartChat: User exists");
                    returnData.put("Result", "Chat already exists");
                    returnData.put("Code", "201");
                    returnData.put("CID", getChat.get("CID"));
                    returnData.put("Time_Started", getChat.get("Time_Started"));
                }
            } catch(Exception e){
                returnData.put("Result", e.getMessage());
                returnData.put("Code", "201");
            }
            return returnData;
        } else {
            returnData.put("Result", "Empty field given");
            returnData.put("Code", "210");
            return returnData;
        }
    }

    public Map getChats(String id){
        if(id != null && id != ""){
            Map returnData = new HashMap<>();
            try {
                Map <String, String> data = db.getChats(id);

                returnData.put("Chats", data);
                returnData.put("Result", "Ok");
                returnData.put("Code", "0");
            } catch(Exception e){
                System.out.println("GetChats: Exception: " + e.getMessage());
                returnData.put("Result", e.getMessage());
                returnData.put("Code", "311");
            }
            return returnData;
        } else {
            Map<String, String> data = new HashMap<>();
            data.put("Result", "No id given");
            data.put("Code", "310");
            return data;
        }
    }

    public Map send(String cid, String uid, String message){
        Map<String, String> returnData = new HashMap();
        if(cid != null && cid != "" && uid != null && uid != "" && message != null && message != ""){
            try {
                Long msgId = db.insertChatregel(cid, uid, message);
                if(msgId != null){
                    returnData.put("Result", "Message send");
                    returnData.put("Code", "0");
                } else {
                    returnData.put("Result", "Message send failed");
                    returnData.put("Code", "412");
                }

            } catch(Exception e){
                System.out.println("Send: Exception: " + e.getMessage());
                returnData.put("Result", e.getMessage());
                returnData.put("Code", "411");
            }
        } else {
            returnData.put("Result", "Empty field given");
            returnData.put("Code", "410");
        }
        return returnData;
    }

    public Map getMessages(String cid){
        if(cid != null){
            Map returnData = new HashMap<>();
            try {
                Map<String, String> data = db.getChatregels(cid);
                Map<String, String> chatData = db.getChatByID(cid);

                returnData.put("Messages", data);
                returnData.put("Chat", chatData);
                returnData.put("Result", "Ok");
                returnData.put("Code", "0");
            } catch(Exception e){
                System.out.println("GetMessages: Exception: " + e.getMessage());
                returnData.put("Result", e.getMessage());
                returnData.put("Code", "511");
            }
            return returnData;
        } else {
            Map<String, String> data = new HashMap<>();
            data.put("Result", "No ChatId given");
            data.put("Code", "510");
            return data;
        }
    }
}
