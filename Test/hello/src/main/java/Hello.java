/**
 * Created by michel on 30-5-2016.
 */
import static spark.Spark.get;
public class Hello {
    public static void main(String[] args){
        get("/hello", (req, res) -> "Hello");
    }
}
