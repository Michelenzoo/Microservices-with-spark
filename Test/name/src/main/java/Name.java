/**
 * Created by michel on 30-5-2016.
 */
import static spark.Spark.get;
public class Name {
    public static void main(String[] args){
        get("/name/:name", (request, response) -> {
            return request.params(":name");
        });
    }
}
